<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = "wishlists";
    protected $fillable = ['product_id','user_id'];

    public function products(){
    	$this->belongsTo('App\Product');
    }
    public function user(){
    	$this->belongsTo('App\User');
    }
}
