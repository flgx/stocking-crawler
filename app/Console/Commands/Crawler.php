<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Goutte;
use App\Product;
use Input;
class Crawler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for new products on www.appliancesdelivered.ie if are some one new add it to our database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crawler = \Goutte::request('GET', 'https://www.appliancesdelivered.ie/search?sort=price_desc'); //crawler url
        $title='';
        $url='';
        $image='';
        $price='';
        $curr="€";
        $totalpages= filter_var($crawler->filter('.result-list-pagination a')->last()->attr('href'), FILTER_SANITIZE_NUMBER_INT);//all pages
        $i=1;//global counter
        $addcount=0; //count additions to database.
        $dbproducts = array();
        $dbproducts = Product::orderBy('price','DESC')->pluck('url')->toArray(); // get all products on db
        while($i <= $totalpages){
            $crawlero = \Goutte::request('GET', "https://www.appliancesdelivered.ie/search?sort=price_desc&page=".$i.""); //loop every page

            $crawlero->filter('.search-results-product')->each(function ($node) use (&$dbproducts,&$addcount)  { //crawler filter start              
                $title=$node->filter('.product-description .row .col-xs-12 h4 a')->text(); //product title
                $url=$node->filter('.product-image')->children()->attr('href');//product url
                $image=$node->filter('.product-image')->children()->children()->attr('src');//product featured image
                $price=$node->filter('.product-description .section-title')->text();//product price
                $nice = str_replace('€','', str_replace(",", "",$price));          
                if(!in_array($url, $dbproducts)){ //check if the product exist on our db
                    //Create new product instance
                    $product = new Product();
                    //Set fields
                    $product->title = $title;
                    $product->url= $url;
                    $product->image = $image;
                    $product->price = $nice;
                    $product->save();
                    $addcount++;                   
                }
        });
        $i++;//counter increments
        } //end crawler filter
        if($addcount > 0){//if found new products       
        $this->info("We found ".$addcount." new products and they have been added on our database.");
        }else{          
        $this->info("We don't found any new product.");
        }
    }
}
