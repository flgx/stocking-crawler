<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";

    protected $fillable = ['name'];

    public function club(){
    	
    	return $this->belongsTo('App\Club');
    }
    public function post(){
    	
    	return $this->belongsTo('App\Post');
    }
}
