<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Wishlist;
use Laracasts\Flash\Flash;
use App\Product;
use Auth;
class Wishlistscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wishlist=array();
        if(Auth::check()){
        //get user wishlist to check later if the product is added or not.         
        $wishlist = Wishlist::orderBy('id','DESC')->where('user_id',Auth::user()->id)->pluck('product_id')->toArray();
        } 
        //get all products
        $allproducts= Product::orderBy('id','DESC')->get();        
        //store all products on user wishlist
        $products = array();
        //check what products have the user on his wishlist.
        foreach($allproducts as $product){
            if(in_array($product->id,$wishlist)){
                $products[]=$product;
            }
        }
        return view('wishlists.index')->with('products',$products)->with('wishlist',$wishlist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wishlist = new Wishlist();
        $wishlist->product_id=$request->product_id;
        $wishlist->user_id=$request->user_id;
        $wishlist->save();
        Flash::success('Product added to your wishlist');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wishlistid = Wishlist::where('product_id',$id)->pluck('id')->first();
        $wishlist = Wishlist::find($wishlistid);
        $wishlist->delete();        
        Flash::error('Product deleted from your wishlist');
        return redirect()->back();
    }
}
