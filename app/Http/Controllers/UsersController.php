<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Laracasts\Flash\Flash;
use App\Image;
use Form;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        $user = User::find($id);
        $user->fill($request->all()); 
        if($request->logo != ''){
            $file = $request->logo;
            $filename = $file->getClientOriginalName();


            $extension = $file->getClientOriginalExtension();
            $picture = date('His').'_'.$filename;
            //make profile images
            $image=\Image::make($file->getRealPath()); //Call image library installed.
            $destinationPath = 'img/users/';
            $image->resize(null,600, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($destinationPath.'logo_'.$picture);


            //make images thumbnails   
            $image2=\Image::make($file->getRealPath()); //Call immage library installed.      
                               
            $thumbPath ='img/users/thumbs/';
            $image2->resize(100, 100);
            $image2->save($thumbPath.'thumb_'.$picture);

        }
        $user->logo=$picture;
        $user->save();
        Flash::success('User updated');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
