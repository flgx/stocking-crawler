<?php 
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Wishlist;
use Auth;
class WishlistComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::check()){            
        $wishlists = Wishlist::where('user_id',Auth::user()->id)->get();
        $view->with('count', $wishlists->count());
        }
    }

}