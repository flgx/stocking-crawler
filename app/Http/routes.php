<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/',[
	'uses' => 'ProductsController@index',
	'as'   => 'products.index',
]);
Route::get('/price_order/{order?}',[
	'uses' => 'ProductsController@index',
	'as'   => 'products.index',
]);

Route::get('/setproducts/now',[
	'uses' => 'ProductsController@setProducts',
	'as'   => 'products.setproducts',
]);
Route::auth();
Route::group(['prefix' => 'admin','middleware' => 'auth'], function () {
	Route::resource('wishlists', 'WishlistsController');
	Route::resource('users', 'UsersController');
});
Route::get('/home', 'HomeController@index');
