@extends('layouts.front')

@section('content')
{!! Form::open(['url' => 'admin/users/'.$user->id,'method' => 'PUT','files'=>'true']) !!}
							    {{ csrf_field() }}
	<h2>{{$user->name}}<img src="{{asset('img/users/thumbs/thumb_'.$user->logo)}}" class=" img-circle img-responsive" alt="Stocking"></h2>
	
	<div class="form-group">
		<label for="name">Username</label>
		<input type="text" class="form-control" name="name" value="{{$user->name}}">
	</div>
	<div class="form-group">
		<label for="email">Email</label>
		<input type="text" class="form-control" name="email" value="{{$user->email}}">
	</div>
	<div class="form-group">
		<label for="logo">Profile Image</label>
		@if($user->logo != "")
		@endif
		<input type="file" name="logo" class="form-control">
	</div>
	<button type="submit" class="btn btn-warning">Edit Profile</button>
{!! Form::close() !!}


@endsection