@extends('layouts.front')

@section('content')
	@foreach($products as $product)
	<div class="search-results-product row">

	    <div class="product-image col-xs-4 col-sm-4">
	        <a href="{{$product['url']}}">
	            <img class="img-responsive thumb"  src="{{$product['image']}}" alt="{{$product['title']}}">
	        </a>
	    </div>
	    <div class="product-description col-xs-8 col-sm-8">
	        <div class="row">
	            <div class="col-xs-12 col-sm-7 col-lg-8">
		            <a href="{{$product['url']}}">
		                    <img class="article-brand" src="{{$product['brandimage']}}">
		            </a>                
		            <h4>
		            	<a href="{{$product['url']}}">{{$product['title']}}</a>
		            </h4>
		            <ul class="result-list-item-desc-list hidden-xs">
		                <li>Capacity: 449L/122L</li>
		                <li>Split Type: 70/30</li>
		                <li>Frost Free: Yes</li>
		                <li>Energy Rating: A+</li>
		            </ul>
	            </div>
	            <div class="col-xs-12 col-sm-5 col-lg-4">
	                <div class="col-xs-12">
	                    <h3 class="section-title">{{ Currency::format($product->price,'USD')}}</h3>
	                </div>

	                <div class="col-xs-12">
	                	@if(Auth::check())
	                	@if(!in_array($product['id'],$wishlist))
	                    <form action="{{url('/admin/wishlists')}}" method="post">
	                        {{ csrf_field() }}
	                        <input type="hidden" name="product_id" value="{{$product['id']}}">
	                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
	                        <button type="submit" class="btn btn-primary btn-block buy-now-btn"><i class="fa fa-heart"></i> Add To Your Wishlist</button>
	                    </form>
	                    @else							
	                    <form action="{{url('/admin/wishlists/'.$product['id'])}}" method="POST">
						    {{ method_field('DELETE') }}
						    {{ csrf_field() }}
	                        <button type="submit" class="btn btn-danger btn-block buy-now-btn"><i class="fa fa-times"></i> Remove From Your Wishlist</button>
	                    </form>
	                    @endif     
	                    <div class="clearfix"></div>
	                    @endif
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	@endforeach
@endsection