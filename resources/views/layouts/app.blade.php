<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Stocking Crawler / Admin Panel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
        <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-left" href="{{url('/')}}"><img class="img-responsive" width="250" src="{{asset('img/logo.png')}}" alt=""></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
              <li {{ (Request::is('price_order/DESC') || Request::is('price_order/ASC')   ? 'class=active' : '') }}><a href="{{url('/')}}" ><i class="fa fa-home"></i>  Home</a></li>
              @if(Auth::check())
              <li {{ (Request::is('admin/wishlists') ? 'class=active' : '') }}><a href="{{url('/admin/wishlists')}}" ><i class="fa fa-heart"></i> Wishlist</a></li>
              <li {{ (Request::is('admin/user') ? 'class=active' : '') }}><a href="{{url('/admin/users/'.Auth::user()->id.'/edit')}}" ><i class="fa fa-user"></i> {{Auth::user()->name}}</a></li>
              @else

              <li><a href="{{ url('/login') }}" {{ (Request::is('login') ? 'class=active' : '') }}><i class="fa fa-user"></i> Login</a></li>
              <li><a href="{{ url('/register') }}" {{ (Request::is('register') ? 'class=active' : '') }}><i class="fa fa-user"></i> Register</a></li>
              @endif
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

                <div class="col-xs-12">                
                 @include('flash::message')

                </div> 
    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
