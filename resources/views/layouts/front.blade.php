<!DOCTYPE html>
<html>
    <head>
        <title>Stocking Crawler</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        @yield('css')
    </head>
    <body>      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-left" href="{{url('/')}}"><img class="img-responsive" width="250" src="{{asset('img/logo.png')}}" alt=""></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
              <li {{ (Request::is('price_order/DESC') || Request::is('price_order/ASC')   ? 'class=active' : '') }}><a href="{{url('/')}}" ><i class="fa fa-home"></i>  Home</a></li>
                  @if(Auth::check())
              <li {{ (Request::is('admin/wishlists') ? 'class=active' : '') }}><a href="{{url('/admin/wishlists')}}" ><i class="fa fa-heart"></i> Wishlist ({{$count}})</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> {{Auth::user()->name}} <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{url('/admin/users/'.Auth::user()->id.'/edit')}}">Profile</a></li>
                  <li><a href="{{url('/logout')}}">Logout</a></li>
                </ul>
              </li>
              @else

              <li><a href="{{ url('/login') }}"><i class="fa fa-user"></i> Login</a></li>
              <li><a href="{{ url('/register') }}"><i class="fa fa-user"></i> Register</a></li>
              @endif
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
        <div class="container">
            <div class="col-xs-12">                
             @include('flash::message')

            </div> 
            @yield('content')
        </div>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
    //I have problems with currency symbol. So my fix is this. Replace $ for Euro symbol.
    $(".section-title").each(function() {
    var text = $(this).text();
    text = text.replace("$", "€");
    $(this).text(text);
    });
    </script>
    @yield('js')
    </body>

</html>
