#Stocking Crawler
- Create new db and set up on your .env file.
- Run "php artisan migrate" to import all tables.
- Run "php artisan composer install" to install all the dependencies.
- Run "php vendor:publish" to setup some configurations.
- Execute "php artisan crawler:scan" to start the product crawler. A cron job is set to do this every day and check for new products.

